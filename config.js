qs.relax.RelaxContants.GAME_ID = "bigbadwolf90";

modulesConfig.featuremanager = {
    release: {
        bootstrap: "com.quickspin.featuremanager.FeatureManagerBootstrap",
        resources: {
            code: {
                URL: "../qs-libs/qs-meta-featuremanager/com.quickspin.web.meta.qs-meta-featuremanager.min.js"
            }
        }
    },
    debug: {
        bootstrap: "com.quickspin.featuremanager.FeatureManagerBootstrap",
        resources: {
            code: {
                URL: "../qs-libs/qs-meta-featuremanager/com.quickspin.web.meta.qs-meta-featuremanager.js"
            }
        }
    }
};

modulesConfig.challenges = {
    release: {
        bootstrap: "com.quickspin.challenges.ChallengeBootstrap",
        resources: {
            code: {
                URL: "../qs-libs/qs-meta-challenges/v1/com.quickspin.web.meta.qs-meta-challenges.min.js"
            }
        }
    },
    debug: {
        bootstrap: "com.quickspin.challenges.ChallengeBootstrap",
        resources: {
            code: {
                URL: "../qs-libs/qs-meta-challenges/v1/com.quickspin.web.meta.qs-meta-challenges.js"
            }
        }
    }
};

modulesConfig.norskTippingBridge = {
    enabled: "false" === "true",
    release: {
        bootstrap: "com.quickspin.norsktippingbridge.NorskTippingBridgeBootstrap",
        resources: {
            code: {
                URL: "../qs-libs/qs-norsktipping-bridge/com.quickspin.web.qs-norsktipping-bridge.min.js"
            }
        }
    },
    debug: {
        bootstrap: "com.quickspin.norsktippingbridge.NorskTippingBridgeBootstrap",
        resources: {
            code: {
                URL: "../qs-libs/qs-norsktipping-bridge/com.quickspin.web.qs-norsktipping-bridge.js"
            }
        }
    }
};

modulesConfig.gameEvents.debug.resources.code.URL = "../qs-libs/gameevents/com.quickspin.web.gameevents.js";
modulesConfig.gameEvents.release.resources.code.URL = "../qs-libs/gameevents/com.quickspin.web.gameevents.min.js";
modulesConfig.metaAPI.debug.resources.code.URL = "../qs-libs/qs-meta-api/com.quickspin.web.meta.qs-meta-api.js";
modulesConfig.metaAPI.release.resources.code.URL = "../qs-libs/qs-meta-api/com.quickspin.web.meta.qs-meta-api.min.js";

// modules - set state
if ("false" === "true") {
    modulesConfig.gameEvents.enabled = false;
    modulesConfig.metaAPI.enabled = false;
    modulesConfig.achievements.enabled = false;
    modulesConfig.achievementsKeypad.enabled = false;
    modulesConfig.challenges.enabled = false;
    modulesConfig.featuremanager.enabled = false;
} else {
    modulesConfig.gameEvents.enabled = true;
    modulesConfig.metaAPI.enabled = true;
    modulesConfig.achievements.enabled = true;
    modulesConfig.achievementsKeypad.enabled = true;
    modulesConfig.challenges.enabled = true;
    modulesConfig.featuremanager.enabled = true;
}