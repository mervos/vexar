var modulesConfig = {
    capabilities: {
        enabled: true,
        release: {
            bootstrap: "qs.core.capabilities.CapabilitiesBootstrap",
            resources: {
                code: {
                    URL: "capabilities/com.quickspin.web.capabilities.min.js",
                    type: "js"
                }
            }
        },
        debug: {
            bootstrap: "qs.core.capabilities.CapabilitiesBootstrap",
            resources: {
                code: {
                    URL: "capabilities/com.quickspin.web.capabilities.js",
                    type: "js"
                }
            }
        }
    },
    realityCheck: {
        enabled: true,
        release: {
            bootstrap: "qs.core.realitycheck.RealityCheckBootstrap",
            resources: {
                code: {
                    URL: "../qs-libs/reality-check/com.quickspin.web.extensions.reality-check.min.js"
                }
            }
        },
        debug: {
            bootstrap: "qs.core.realitycheck.RealityCheckBootstrap",
            resources: {
                code: {
                    URL: "../qs-libs/reality-check/com.quickspin.web.extensions.reality-check.js"
                }
            }
        }
    },
    gameEvents: {
        enabled: false,
        release: {
            bootstrap: "com.quickspin.game_events.GameEventsBootstrap",
            resources: {
                code: {
                    URL: "gameevents/com.quickspin.web.gameevents.min.js",
                    type: "js"
                }
            }
        },
        debug: {
            bootstrap: "com.quickspin.game_events.GameEventsBootstrap",
            resources: {
                code: {
                    URL: "gameevents/com.quickspin.web.gameevents.js",
                    type: "js"
                }
            }
        }
    },
    metaAPI: {
        enabled: false,
        release: {
            bootstrap: "com.quickspin.web.meta.api.MetaAPIBootstrap",
            resources: {
                code: {
                    URL: "qs-meta-api/com.quickspin.web.meta.qs-meta-api.min.js"
                }
            }
        },
        debug: {
            bootstrap: "com.quickspin.web.meta.api.MetaAPIBootstrap",
            resources: {
                code: {
                    URL: "qs-meta-api/com.quickspin.web.meta.qs-meta-api.js"
                }
            }
        }
    },
    analytics: {
        enabled: false,
        release: {
            bootstrap: "qs.utils.analytics.QSAnalyticsBootstrap",
            resources: {
                code: {
                    URL: "qs-utils-analytics/com.quickspin.web.qs-utils-analytics.min.js",
                    type: "js"
                }
            }
        },
        debug: {
            bootstrap: "qs.utils.analytics.QSAnalyticsBootstrap",
            resources: {
                code: {
                    URL: "qs-utils-analytics/com.quickspin.web.qs-utils-analytics.js",
                    type: "js"
                }
            }
        }
    },
    achievementsKeypad: {
        enabled: true,
        release: {
            bootstrap: "com.quickspin.keypad.achievements.AchievementsKeypadModule",
            resources: {
                looks: {
                    type: "css",
                    URL: "keypad-dom-achievements/assets/hamlin/hamlin_desktop.min.css"
                },
                code: {
                    URL: "keypad-dom-achievements/com.quickspin.web.extensions.keypad.keypad-dom-achievements.min.js"
                }
            }
        },
        debug: {
            bootstrap: "com.quickspin.keypad.achievements.AchievementsKeypadModule",
            resources: {
                looks: {
                    type: "css",
                    URL: "keypad-dom-achievements/assets/hamlin/hamlin_desktop.css",
                },
                code: {
                    URL: "keypad-dom-achievements/com.quickspin.web.extensions.keypad.keypad-dom-achievements.js"
                }
            }
        }
    },
    achievements: {
        enabled: true,
        release: {
            bootstrap: "com.quickspin.achievements.AchievementModuleBootstrap",
            resources: {
                code: {
                    URL: "qs-core-achievements/com.quickspin.web.qs-core-achievements.min.js"
                }
            }
        },
        debug: {
            bootstrap: "com.quickspin.achievements.AchievementModuleBootstrap",
            resources: {
                code: {
                    URL: "qs-core-achievements/com.quickspin.web.qs-core-achievements.js"
                }
            }
        }
    },
    manipulator: {
        enabled: false,
        release: {
            bootstrap: "qs.utils.manipulator.ManipulatorBootstrap",
            resources: {
                code: {
                    URL: "qs-utils-manipulator/com.quickspin.web.qs-utils-manipulator.min.js",
                    type: "js"
                },
                looks: {
                    URL: "qs-utils-manipulator/assets/qs-utils-manipulator.min.css",
                    type: "css"
                }
            }
        },
        debug: {
            bootstrap: "qs.utils.manipulator.ManipulatorBootstrap",
            resources: {
                code: {
                    URL: "qs-utils-manipulator/com.quickspin.web.qs-utils-manipulator.js",
                    type: "js"
                },
                looks: {
                    URL: "qs-utils-manipulator/assets/qs-utils-manipulator.css",
                    type: "css"
                }
            }
        }
    },
    tournaments: {
        enabled: false,
        release: {
            bootstrap: "MetaTournaments.Widget",
            resource: {
                code: {
                    URL: "../com.quickspin.web.meta.qs-meta-tournaments.min.js",
                    type: "js"
                }
            }
        },
        debug: {
            bootstrap: "MetaTournaments.Widget",
            resources: {
                code: {
                    URL: "../com.quickspin.web.meta.qs-meta-tournaments.js",
                    type: "js"
                }
            }
        }
    }
};

qs.relax.RelaxContants.GAME_ID = "html5bbwolf90";
qs.relax.RelaxContants.GAME_MODE = "intg";
qs.relax.RelaxContants.PARTNER_ID = "1";
qs.relax.RelaxContants.LOGIN_CHANNEL_PARAM = "web";
qs.relax.RelaxContants.IMMEDIATE_PAYOUTS = "false";
qs.relax.RelaxContants.END_POINT = "https://casino-client-api.intstage.qs-gaming.net";
qs.relax.RelaxContants.END_POINT_TOKEN = "https://casino-client-api.intstage.qs-gaming.net";
qs.relax.RelaxContants.END_POINT_REPLAY = "https://casino-client-api.intstage.qs-gaming.net";

qs.relax.RelaxContants.TICKET_EXTENSION = "/capi/1.0/casino/token/gettoken?channel=web";
qs.relax.RelaxContants.LOGIN_EXTENSION = "/game/rmlogin/";
qs.relax.RelaxContants.FUN_LOGIN_EXTENSION = "/game/demormlogin/";
qs.relax.RelaxContants.SPIN_EXTENSION = "/game/play/";
qs.relax.RelaxContants.BALANCE_EXTENSION = "/game/getbalance/";
qs.relax.RelaxContants.PICK_EXTENSION = "/game/play/";
qs.relax.RelaxContants.REFRESH_EXTENSION = "/game/refresh/";
qs.relax.RelaxContants.PING_EXTENSION = "/game/ping/";
qs.relax.RelaxContants.BET_AMOUNTS_EXTENSION = "/game/limits/";
qs.relax.RelaxContants.FREEROUNDS_EXTENSION = "/game/freespins/";
qs.relax.RelaxContants.REPLAY_EXTENSION = "/capi/1.0/casino/replay/getreplay";
qs.relax.RelaxContants.RESTORE_EXTENSION = "/game/setrestorestate/";
qs.relax.RelaxContants.RESTORE_FINISHED_EXTENSION = "/game/gamefinished/";



qs.system.EnvVars.readFromGetQuery();

qs.system.EnvVars.write("EnableQuickstop", "true");
qs.system.EnvVars.write("EnableFastPlay", "true");

qs.system.EnvVars.write(qs.integration.DefaultGetQueryKey.MARKET_CONFIG, JSON.stringify({
    "market": {
        "gb": {
            "autoplayOptions": [5, 10, 25, 75, 100],
            "lossLimitOptions": [5, 20, 50, 99],
            "singleWinLimitOptions": [10, 20, 75, -1],
            "forceAdvancedAutoplay": true
        },
        "default": {
            "autoplayOptions": [10, 25, 50, 75, 100, 500, 1000],
            "lossLimitOptions": [5, 20, 50, -1],
            "singleWinLimitOptions": [10, 20, 75, -1],
            "forceAdvancedAutoplay": false
        }
    }
}));

qs.system.EnvVars.write(qs.integration.DefaultGetQueryKey.CURRENCY_FORMATTING, {
    "isISOCurrencyPrefix": false,
    "isSymbolCurrencyPrefix": true,
    "ISOToValueSpace": " ",
    "symbolToValueSpace": "",
    "trailingZeros": true,
    "isThousandsSeparated": false,
    "thousandsSeperator": ",",
    "useSymbolIfAvailable": true,
    "decimalSeparator": ".",
    "fractionalDigits": 2,
    "ISOToSymbol": {
        "EUR": "€",
        "USD": "$",
        "GBP": "£"
    }
});